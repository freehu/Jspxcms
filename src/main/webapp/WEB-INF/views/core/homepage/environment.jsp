<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/head.jsp" />
</head>
<body class="skin-blue content-body">
<div class="content-header">
    <h1>系统信息</h1>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <i class="glyphicon glyphicon-stats"></i>
                    <h3 class="box-title">访问统计</h3>
                </div>
                <div class="box-body">
                    <div id="visitChart" style="height:300px;"></div>
                </div>
            </div>
        </div>
        <script>
            var chart = echarts.init(document.getElementById('visitChart'));
            var option = {
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data:['<s:message code="visitLog.pv"/>','<s:message code="visitLog.uv"/>','<s:message code="visitLog.ip"/>']
                },
                grid: {
                    left: '2%',
                    right: '3%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        <c:forEach var="bean" varStatus="status" items="${visitList}">
                        <c:choose>
                        <c:when test="${groupBy=='hour'}">'<fmt:formatDate value="${bean[0]}" pattern="HH"/>H'</c:when>
                        <c:when test="${groupBy=='minute'}">'<fmt:formatDate value="${bean[0]}" pattern="mm"/>m'</c:when>
                        <c:otherwise>'<fmt:formatDate value="${bean[0]}" pattern="MM-dd"/>'</c:otherwise>
                        </c:choose><c:if test="${!status.last}">,</c:if>
                        </c:forEach>
                    ]
                },
                yAxis: {
                    type: 'value'
                },
                series: [
                    {
                        name:'<s:message code="visitLog.pv"/>',
                        type:'line',
                        data:[
                            <c:forEach var="bean" varStatus="status" items="${visitList}">
                            <c:out value="${bean[1]}"/><c:if test="${!status.last}">,</c:if>
                            </c:forEach>
                        ]
                    },
                    {
                        name:'<s:message code="visitLog.uv"/>',
                        type:'line',
                        data:[
                            <c:forEach var="bean" varStatus="status" items="${visitList}">
                            <c:out value="${bean[2]}"/><c:if test="${!status.last}">,</c:if>
                            </c:forEach>
                        ]
                    },
                    {
                        name:'<s:message code="visitLog.ip"/>',
                        type:'line',
                        data:[
                            <c:forEach var="bean" varStatus="status" items="${visitList}">
                            <c:out value="${bean[3]}"/><c:if test="${!status.last}">,</c:if>
                            </c:forEach>
                        ]
                    }
                ]
            };
            chart.setOption(option);
        </script>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th style="width:30%">操作系统</th>
                            <td>${props['os.name']} ${props['os.version']}</td>
                        </tr>
                        <tr>
                            <th style="width:30%">JAVA运行环境</th>
                            <td>${props['java.runtime.name']} ${props['java.runtime.version']}</td>
                        </tr>
                        <tr>
                            <th style="width:30%">JAVA虚拟机</th>
                            <td>${props['java.vm.name']} ${props['java.vm.version']}</td>
                        </tr>
                        <tr>
                            <th style="width:30%">系统用户</th>
                            <td>${props['user.name']}</td>
                        </tr>
                        <tr>
                            <th style="width:30%">用户主目录</th>
                            <td>${props['user.home']}</td>
                        </tr>
                        <tr>
                            <th style="width:30%">用户工作目录</th>
                            <td>${props['user.dir']}</td>
                        </tr>
                        <tr>
                            <th style="width:30%">用户临时目录</th>
                            <td>${props['java.io.tmpdir']}</td>
                        </tr>
                        <tr>
                            <th style="width:30%">最大内存</th>
                            <td><fmt:formatNumber value="${maxMemoryMB}" pattern="#.00" /> MB</td>
                        </tr>
                        <tr>
                            <th style="width:30%">已用内存</th>
                            <td><fmt:formatNumber value="${usedMemoryMB}" pattern="#.00" /> MB</td>
                        </tr>
                        <tr>
                            <th style="width:30%">可用内存</th>
                            <td><fmt:formatNumber value="${useableMemoryMB}" pattern="#.00" /> MB</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="fa fa-safari"></i>
                    <h3 class="box-title">浏览器统计</h3>
                </div>
                <div class="box-body">
                    <div id="browserChart" style="height:260px;"></div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var chart = echarts.init(document.getElementById('browserChart'));
            var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: [
                        <c:forEach var="bean" varStatus="status" items="${browserList}">
                        <c:if test="${status.count < 10}">
                        <c:choose>
                        <c:when test="${'UNKNOWN' eq bean[0]}">'<s:message code="visitLog.UNKNOWN"/>'</c:when>
                        <c:otherwise>'<c:out value="${bean[0]}"/>'</c:otherwise>
                        </c:choose><c:if test="${!status.last}">, </c:if>
                        </c:if>
                        </c:forEach>
                        <c:if test="${fn:length(browserList)>=10}">'<s:message code="visitLog.OTHER"/>'</c:if>
                    ]
                },
                series: [
                    {
                        name: '<s:message code="visitLog.browser"/>',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: [
                            <c:set var="count" value="${0}"/>
                            <c:forEach var="bean" varStatus="status" items="${browserList}">
                            <c:choose>
                            <c:when test="${status.count < 10}">
                            {value:${bean[1]}, name: '<c:choose><c:when test="${'UNKNOWN' eq bean[0]}"><s:message code="visitLog.UNKNOWN"/></c:when><c:otherwise><c:out value="${bean[0]}"/></c:otherwise></c:choose>'}<c:if test="${!status.last}">, </c:if>
                            </c:when>
                            <c:otherwise>
                            <c:set var="count" value="${count+bean[1]}"/>
                            </c:otherwise>
                            </c:choose>
                            </c:forEach>
                                <c:if test="${fn:length(browserList)>=10}">{value:${count}, name: '<s:message code="visitLog.OTHER"/>'}</c:if>
                        ],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            chart.setOption(option);
        </script>
    </div>
</div>
</body>
</html>